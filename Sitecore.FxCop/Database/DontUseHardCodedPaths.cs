﻿namespace Sitecore.FxCop.Database
{
    using Microsoft.FxCop.Sdk;

    /// <summary>
    /// Detects literals containing sitecore paths
    /// </summary>
    internal sealed class DontUseHardCodedPaths : SitecoreBaseRule
    {
        public DontUseHardCodedPaths()
            : base("DontUseHardCodedPaths")
        {
        }

        public override ProblemCollection Check(Member member)
        {
            Method method = member as Method;
            if (method != null)
            {
                this.Visit(method.Body);
            }

            return this.Problems;
        }

        public override void VisitLiteral(Literal literal)
        {
            var value = literal.Value;
            if (value == null)
            {
                return;
            }

            if (value is string)
            {
                if (CheckValue(value as string))
                {
                    this.Problems.Add(new Problem(this.GetResolution(value), literal));
                }
            }
        }

        private bool CheckValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            return value.StartsWith("/sitecore/");
        }

    }
}
